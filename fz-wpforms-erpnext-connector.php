<?php
/**
 * Wordpress plugin to connect WPForms with ERPNext
 *
 * @category  FZ
 * @package   WpformsErpnextConnector
 * @author    "Florian Zeidler <x-fz-wpforms-erpnext-connector@zeidler.ms>"
 * @copyright 2020 Florian Zeidler
 * @license   GPL-3.0-or-later https://www.gnu.org/licenses/gpl-3.0.en.html
 * @link      https://gitlab.com/fzeidler/wpforms-erpnext-connector
 *
 * @wordpress-plugin
 * Plugin Name: fz WPForms ERPNext connector
 * Author: Florian Zeidler
 * Description: Send WPForm Entries into ERPNext.
 * Requires at least: 5.2
 * Version: 1.3.2
 * Text Domain: fz-wpforms-erpnext-connector
 * Domain Path: languages
 */

// Exit if accessed directly.
if (! defined('ABSPATH')) {
    exit;
};

if (!function_exists('FZ_Write_log')) {
    /**
     * Write a message to debug log if enabled
     *
     * @param object $log         string or object to output into log
     * @param int    $debug_level current debug level
     * @param int    $status_code status code of the request (if one was made)
     * @param object $request     full request object (if one was made)
     *
     * @return nothing
     */
    function FZ_Write_log($log, $debug_level = 1, $status_code = 200, $request = [])
    {
        if (false === WP_DEBUG || $debug_level == 0) {
            return;
        }

        $is_error = is_wp_error($request) || $status_code >= 400;

        if ($debug_level > 1) {
            if (is_array($log) || is_object($log)) {
                error_log(print_r($log, true));
            } else {
                error_log("fz-wpforms-erpnext-connector: ".$log);
            }
            error_log("fz-wpforms-erpnext-connector: status code was ".$status_code);
        }

        if ($is_error) {
            // extract ERPNext error message from HTML
            $matches = [];
            $error_message = preg_match(
                '/<pre>(Traceback.*)</pre>',
                $request['response']['message'], $matches
            );
            error_log(
                "fz-wpforms-erpnext-connector: error message:\n".$error_message
            );
        }

        if (!empty($request['response']['message']) && ($debug_level > 2 || ($debug_level > 1 && $is_error))) {
            error_log("fz-wpforms-erpnext-connector: full response:\n".$request['response']['message']);
        }

    }
}

if (!function_exists('FZ_ERPNext_Print_fields')) {
    /**
     * Print WPForms fields with label and value to a string
     *
     * @param array $fields WPForms fields array
     *
     * @return string representation of all fields and their values
     */
    function FZ_ERPNext_Print_fields($fields)
    {
        $output = '';
        foreach ($fields as $field) {
            if ($field['value']) {
                $output = $output . $field['name'] . ":\n" . esc_html($field['value']) . "\n\n";
            }
        }
        return $output;
    }
}

if (!function_exists('FZ_ERPNext_Find_existing')) {
    /**
     * Check if a Document with the given field set to given value exists.
     * For example: does a Lead with this email exist already?
     *
     * @param string $domain      your ERPNext domain name
     * @param string $api_key     your ERPNext api key
     * @param string $api_secret  your ERPNext api secret/password
     * @param string $doctype     ERPNext DocType name
     * @param string $fieldname   DocType Field to filter on
     * @param string $value       value of field to filter on
     * @param int    $debug_level verbosity of debug.log messages
     *
     * @return string empty string if nothing found, name of document if it exists
     */
    function FZ_ERPNext_Find_existing($domain, $api_key, $api_secret, $doctype, $fieldname, $value, $debug_level = 1)
    {
        $request = [
            "headers"     => [
                "Content-Type" => "application/json",
                "Authorization" => "Basic " . base64_encode($api_key . ":" . $api_secret),
            ],
            "timeout"     => 120,
        ];


        $url = 'https://' . $domain . '/api/resource/' . $doctype .
               '?filters=%5B%5B"' . $fieldname .
               '","=","' . $value . '"%5D%5D&fields=%5B"name"%5D';

        // Query ERPNext
        $request = wp_remote_get($url, $request);



        if (is_wp_error( $request )) {
            FZ_Write_log($request->get_error_message(),$debug_level,0);
            return '';
        }

        $status_code = $request['response']['code'];


        // log response on error
        if (!($request['response']['code']==200)) {
            if ($debug_level > 1) {
                FZ_Write_log($request['body'], $debug_level, $request['response']['code'], $request);
            }
            return false;
        } else {
            FZ_Write_log(
                "GET request sent to ".$url,
                $debug_level, $status_code, $request
            );
            $body = wp_remote_retrieve_body($request);
            $data = json_decode($body);
            if (is_object($data) && !empty($data->data)) {
                return $data->data[0]->name;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('FZ_ERPNext_Create_Linked_doc')) {
    /**
     * Create a new Document of a given doctype, passing values for up to 3 fields
     * For example: create a Lead with the name foobar:
     * FZ_ERPNext_Create_Linked_doc(
     *   "erpnext.example.com", "<erpnext-api-key>", "<erpnext-api-secret>",
     *   "Lead", "lead_name", "foobar");
     *
     * @param string $domain      your ERPNext domain name
     * @param string $api_key     your ERPNext api key
     * @param string $api_secret  your ERPNext api secret/password
     * @param string $doctype     ERPNext DocType name
     * @param string $fieldname_0 DocType Field to fill $value_1 into
     * @param string $value_0     value for field 0
     * @param string $fieldname_1 DocType Field to fill $value_1 into
     * @param string $value_1     value for field 1
     * @param string $fieldname_2 DocType Field to fill $value_2 into
     * @param string $value_2     value for field 2
     * @param int    $debug_level verbosity of debug.log
     *
     * @return string empty string if error, name of document if successful
     */
    function FZ_ERPNext_Create_Linked_doc(
        $domain, $api_key, $api_secret, $doctype,
        $fieldname_0, $value_0, $fieldname_1, $value_1, $fieldname_2, $value_2,
        $debug_level
    ) {
        $bodydata = [
            $fieldname_0 => $value_0,
            $fieldname_1 => $value_1,
            $fieldname_2 => $value_2,
        ];

        $jsonbody = wp_json_encode($bodydata);

        $request = [
            "body"        => $jsonbody,
            "headers"     => [
                "Content-Type" => "application/json",
                "Authorization" => "Basic " . base64_encode($api_key . ":" . $api_secret),
            ],
            "timeout"     => 120,
        ];

        $url = "https://" . $domain . "/api/resource/" . $doctype;
        // Submit to ERPNext
        $request = wp_remote_post($url, $request);

        if (is_wp_error($request)) {
            FZ_Write_log($request->get_error_message(),$debug_level,0);
            return '';
        }

        // log response on error
        if (!($request['response']['code']==200)) {
            FZ_Write_log(
            "Error: POST request sent to ".$url." status ".$status_code, $debug_level, $request['response']['code'], $request
            );
            return '';
        } else {
            FZ_Write_log(
                "POST request sent to ".$url, $debug_level, $request['response']['code'], $request
            );
            $body = wp_remote_retrieve_body($request);
            $data = json_decode($body);
            if (is_object($data) && !empty($data->data)) {
                return $data->data->name;
            } else {
                return '';
            }
        }
    }
}

if (!function_exists('FZ_ERPNext_send')) {
    /**
     * Send data to ERPNext API
     *
     * @param array $fields    contains the form fields and their values
     * @param array $entry     currently unused
     * @param array $form_data WPForms Dictionary to store form settings and data
     * @param int   $entry_id  currently unused
     *
     * @return nothing
     */
    function FZ_ERPNext_send( $fields, $entry, $form_data, $entry_id )
    {
        // get settings
        $domain = $api_key = $api_secret = $doctype = false;
        if (!empty($form_data['settings']['fz_erpnext_debug_level'])) {
            $debug_level = esc_attr($form_data['settings']['fz_erpnext_debug_level']);
        } else {
            $debug_level = 1;
        }

        if (!empty($form_data['settings']['fz_erpnext_domain'])) {
            $domain = esc_attr($form_data['settings']['fz_erpnext_domain']);
        }

        if (!empty($form_data['settings']['fz_erpnext_api_key'])) {
            $api_key = esc_attr($form_data['settings']['fz_erpnext_api_key']);
        }

        if (!empty($form_data['settings']['fz_erpnext_api_secret'])) {
            $api_secret = esc_attr($form_data['settings']['fz_erpnext_api_secret']);
        }

        if (!empty($form_data['settings']['fz_erpnext_doctype'])) {
            $doctype = esc_attr($form_data['settings']['fz_erpnext_doctype']);
        }

        if (!empty($form_data['settings']['fz_erpnext_linked_doctype'])) {
            $linked_doctype = esc_attr($form_data['settings']['fz_erpnext_linked_doctype']);
        }

        if (!empty($form_data['settings']['fz_erpnext_link_type_field_name'])) {
            $link_type_field_name = esc_attr($form_data['settings']['fz_erpnext_link_type_field_name']);
        }

        if (!empty($form_data['settings']['fz_erpnext_link_field_name'])) {
            $link_field_name = esc_attr($form_data['settings']['fz_erpnext_link_field_name']);
        }
        $linked_field_0 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_fieldname_0'])) {
            $linked_field_0 = esc_attr($form_data['settings']['fz_erpnext_linked_doctype_fieldname_0']);
        }

        $linked_value_0 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_value_0'])) {
            $linked_value_0 = apply_filters(
                'wpforms_process_smart_tags',
                $form_data['settings']['fz_erpnext_linked_doctype_value_0'],
                $form_data, $fields, $entry_id
            );
        }

        $linked_field_1 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_fieldname_1'])) {
            $linked_field_1 = esc_attr($form_data['settings']['fz_erpnext_linked_doctype_fieldname_1']);
        }

        $linked_value_1 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_value_1'])) {
            $linked_value_1 = apply_filters(
                'wpforms_process_smart_tags',
                $form_data['settings']['fz_erpnext_linked_doctype_value_1'],
                $form_data, $fields, $entry_id
            );
        }

        $linked_field_2 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_fieldname_2'])) {
            $linked_field_2 = esc_attr($form_data['settings']['fz_erpnext_linked_doctype_fieldname_2']);
        }

        $linked_value_2 = '';
        if (!empty($form_data['settings']['fz_erpnext_linked_doctype_value_2'])) {
            $linked_value_2 = apply_filters(
                'wpforms_process_smart_tags',
                $form_data['settings']['fz_erpnext_linked_doctype_value_2'],
                $form_data, $fields, $entry_id
            );
        }

        // quit if config is incomplete
        if (!($domain && $api_key && $api_secret && $doctype)) {
            FZ_Write_log("can't send, ".$form_data['settings']['form_title']." config incomplete", $debug_level, 400);
            return;
        }

        // create a linked document first, only if all the settings are present
        if ($linked_doctype && $linked_field_0 && $linked_value_0) {
            // try and load a document with that name
            $linked_name = FZ_ERPNext_Find_existing(
                $domain, $api_key, $api_secret,
                $linked_doctype, $linked_field_0, $linked_value_0,
                $debug_level
            );
            // does the document exist already?
            if ($linked_name && $linked_name != '') {
                $link_field_value = $linked_name;
            } else {
                $link_field_value = FZ_ERPNext_Create_Linked_doc(
                    $domain, $api_key, $api_secret,
                    $linked_doctype, $linked_field_0, $linked_value_0,
                    $linked_field_1, $linked_value_1,
                    $linked_field_2, $linked_value_2,
                    $debug_level
                );
            }
        }

        // Get field ids
        $email_field_id = $form_data['settings']['fz_erpnext_field_email'];
        $name_field_id = $form_data['settings']['fz_erpnext_field_name'];
        $phone_field_id = $form_data['settings']['fz_erpnext_field_phone'];
        $all_fields_fieldname = $form_data['settings']['fz_erpnext_all_fields'];
        $title_fieldname = $form_data['settings']['fz_erpnext_title_field'];

        // TODO turn into an array and allow arbitrarily many custom fields
        $custom_1_src = $form_data['settings']['fz_erpnext_custom_1_src'];
        $custom_1_target = $form_data['settings']['fz_erpnext_custom_1_target'];
        $custom_2_src = $form_data['settings']['fz_erpnext_custom_2_src'];
        $custom_2_target = $form_data['settings']['fz_erpnext_custom_2_target'];
        $custom_3_src = $form_data['settings']['fz_erpnext_custom_3_src'];
        $custom_3_target = $form_data['settings']['fz_erpnext_custom_3_target'];
        $custom_4_src = $form_data['settings']['fz_erpnext_custom_4_src'];
        $custom_4_target = $form_data['settings']['fz_erpnext_custom_4_target'];
        $custom_5_src = $form_data['settings']['fz_erpnext_custom_5_src'];
        $custom_5_target = $form_data['settings']['fz_erpnext_custom_5_target'];
        $custom_6_src = $form_data['settings']['fz_erpnext_custom_6_src'];
        $custom_6_target = $form_data['settings']['fz_erpnext_custom_6_target'];
        $custom_7_src = $form_data['settings']['fz_erpnext_custom_7_src'];
        $custom_7_target = $form_data['settings']['fz_erpnext_custom_7_target'];
        $custom_8_src = $form_data['settings']['fz_erpnext_custom_8_src'];
        $custom_8_target = $form_data['settings']['fz_erpnext_custom_8_target'];
        $custom_9_src = $form_data['settings']['fz_erpnext_custom_9_src'];
        $custom_9_target = $form_data['settings']['fz_erpnext_custom_9_target'];
        $custom_10_src = $form_data['settings']['fz_erpnext_custom_10_src'];
        $custom_10_target = $form_data['settings']['fz_erpnext_custom_10_target'];

        $email = isset($email_field_id) && !empty($email_field_id) ? esc_html($fields[$email_field_id]['value']) : '';
        $name = isset($name_field_id) && !empty($name_field_id) ? esc_html($fields[$name_field_id]['value']) : '';
        $phone = isset($phone_field_id) && !empty($phone_field_id) ? esc_html($fields[$phone_field_id]['value']) : '';

        $bodydata = [
            "status"    => "Open",
            "email_id"  => $email,
            "lead_name" => $name,
            "phone"     => $phone,
            $all_fields_fieldname => FZ_ERPNext_Print_fields($fields),
            "source"    => esc_html($form_data['settings']['fz_erpnext_source']),
            $title_fieldname => esc_html($form_data['settings']['form_title']),
            $link_type_field_name =>  $linked_doctype,
            $link_field_name => $link_field_value,
            $custom_1_target => !empty($custom_1_src) ? apply_filters('wpforms_process_smart_tags', $custom_1_src, $form_data, $fields, $entry_id) : '',
            $custom_2_target => !empty($custom_2_src) ? apply_filters('wpforms_process_smart_tags', $custom_2_src, $form_data, $fields, $entry_id) : '',
            $custom_3_target => !empty($custom_3_src) ? apply_filters('wpforms_process_smart_tags', $custom_3_src, $form_data, $fields, $entry_id) : '',
            $custom_4_target => !empty($custom_4_src) ? apply_filters('wpforms_process_smart_tags', $custom_4_src, $form_data, $fields, $entry_id) : '',
            $custom_5_target => !empty($custom_5_src) ? apply_filters('wpforms_process_smart_tags', $custom_5_src, $form_data, $fields, $entry_id) : '',
            $custom_6_target => !empty($custom_6_src) ? apply_filters('wpforms_process_smart_tags', $custom_6_src, $form_data, $fields, $entry_id) : '',
            $custom_7_target => !empty($custom_7_src) ? apply_filters('wpforms_process_smart_tags', $custom_7_src, $form_data, $fields, $entry_id) : '',
            $custom_8_target => !empty($custom_8_src) ? apply_filters('wpforms_process_smart_tags', $custom_8_src, $form_data, $fields, $entry_id) : '',
            $custom_9_target => !empty($custom_9_src) ? apply_filters('wpforms_process_smart_tags', $custom_9_src, $form_data, $fields, $entry_id) : '',
            $custom_10_target => !empty($custom_10_src) ? apply_filters('wpforms_process_smart_tags', $custom_10_src, $form_data, $fields, $entry_id) : '',
        ];

        // add items if defined in settings
        $items_array = [];
        $fixed_items = $form_data['settings']['fz_erpnext_items_fixed'];
        $conditions = $form_data['settings']['fz_erpnext_items_conditional'];
        if (!empty($fixed_items)) {
            foreach (preg_split("/((\r?\n)|(\r\n?))/", $fixed_items) as $line) {
                $items_array[] = array(
                    "item_code" => $line,
                    "qty" => 1.0
                );
            }
        }

        if (!empty($conditions)) {
            $selected_choices = apply_filters(
                'wpforms_process_smart_tags',
                $form_data['settings']['fz_erpnext_items_conditional_source_fields'],
                $form_data, $fields, $entry_id
            );
            foreach (preg_split("/((\r?\n)|(\r\n?))/", $conditions) as $line) {
                $item_assignment = preg_split('/=>/', $line);
                if (!empty($selected_choices) && strpos($selected_choices, $item_assignment[0]) !== false) {
                    $items_array[] = array(
                        "item_code" => $item_assignment[1],
                        "qty" => 1.0
                    );
                }
            }
        }

        if (!empty($items_array)) {
            $bodydata["with_items"] = 1;
            $bodydata["items"] = $items_array;
        }


        $jsonbody = wp_json_encode($bodydata);

        $request = [
            "body"        => $jsonbody,
            "headers"     => [
                "Content-Type" => "application/json",
                "Authorization" => "Basic " . base64_encode($api_key . ":" . $api_secret),
            ],
            "timeout"     => 120,
        ];

        $url = "https://" . $domain . "/api/resource/" . $doctype;
        // Submit to ERPNext
        $request = wp_remote_post($url, $request);

        if (is_wp_error( $request )) {
            FZ_Write_log($request->get_error_message(),$debug_level,0);
            return '';
        }

        $status_code = $request['response']['code'];
        FZ_Write_log(
            $form_data['settings']['form_title']." request sent to ".$url,
            $debug_level, $status_code, $request
        );
    }
    add_action('wpforms_process_complete', 'FZ_ERPNext_send', 10, 4);
}

if (!function_exists('FZ_ERPNext_WPForms_Settings_content')) {
    /**
     * Add ERPNext settings section to form settings
     *
     * @param array $sections  Dictionary of WPForms settings sections
     * @param array $form_data Processed form data, prepared to be used later.
     *
     * @return nothing
     */
    function FZ_ERPNext_WPForms_Settings_section( $sections, $form_data )
    {
        $sections['fz_erpnext'] = __('ERPNext', 'integrate_erpnext_wpforms');
        return $sections;
    }
    add_filter(
        'wpforms_builder_settings_sections',
        'FZ_ERPNext_WPForms_Settings_section', 200, 20
    );
}

if (!function_exists('FZ_ERPNext_WPForms_Settings_content')) {
    /**
     * ERPNext Settings Content
     * Outputs the required fields for the ERPNext settings section
     *
     * @param object $instance WPForms instance to extend
     *
     * @return nothing
     */
    function FZ_ERPNext_WPForms_Settings_content( $instance )
    {
        echo '<div class="wpforms-panel-content-section wpforms-panel-content-section-fz_erpnext">';
        echo '<div class="wpforms-panel-content-section-title">' . __('ERPNext Connection Settings', 'fz_wpforms_erpnext') . '</div>';
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_domain',
            $instance->form_data,
            __('ERPNext Domain (i.e. erpnext.example.com)', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_api_key',
            $instance->form_data,
            __('ERPNext API Key', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_api_secret',
            $instance->form_data,
            __('ERPNext API Secret', 'fz_wpforms_erpnext')
        );
        echo '</div>';
        echo '<div class="wpforms-panel-content-section wpforms-panel-content-section-fz_erpnext">';
        echo '<div class="wpforms-panel-content-section-title">' . __('Target DocType Settings', 'fz_wpforms_erpnext') . '</div>';
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_doctype',
            $instance->form_data,
            __('ERPNext DocType', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'Opportunity',
                    'fz_wpforms_erpnext'
                ),
            )
        );

        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_source',
            $instance->form_data,
            __('Source', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_title_field',
            $instance->form_data,
            __('Write form title into field', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'select',
            'settings',
            'fz_erpnext_field_name',
            $instance->form_data,
            __('Name', 'fz_wpforms_erpnext'),
            array(
                'field_map'   => array( 'text', 'name' ),
                'placeholder' => __('-- Select Field --', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'select',
            'settings',
            'fz_erpnext_field_email',
            $instance->form_data,
            __('Email Address', 'fz_wpforms_erpnext'),
            array(
                'field_map'   => array( 'email' ),
                'placeholder' => __('-- Select Field --', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'select',
            'settings',
            'fz_erpnext_field_phone',
            $instance->form_data,
            __('Phone Number', 'fz_wpforms_erpnext'),
            array(
                'field_map'   => array( 'phone' ),
                'placeholder' => __('-- Select Field --', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_1_src',
            $instance->form_data,
            __('Custom field 1 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_1_target',
            $instance->form_data,
            __('Custom field 1 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_2_src',
            $instance->form_data,
            __('Custom field 2 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_2_target',
            $instance->form_data,
            __('Custom field 2 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_3_src',
            $instance->form_data,
            __('Custom field 3 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_3_target',
            $instance->form_data,
            __('Custom field 3 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_4_src',
            $instance->form_data,
            __('Custom field 4 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_4_target',
            $instance->form_data,
            __('Custom field 4 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_5_src',
            $instance->form_data,
            __('Custom field 5 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_5_target',
            $instance->form_data,
            __('Custom field 5 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_6_src',
            $instance->form_data,
            __('Custom field 6 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_6_target',
            $instance->form_data,
            __('Custom field 6 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_7_src',
            $instance->form_data,
            __('Custom field 7 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_7_target',
            $instance->form_data,
            __('Custom field 7 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_8_src',
            $instance->form_data,
            __('Custom field 8 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __('enter static text or smart tag', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_8_target',
            $instance->form_data,
            __('Custom field 8 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_9_src',
            $instance->form_data,
            __('Custom field 9 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __('enter static text or smart tag', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_9_target',
            $instance->form_data,
            __('Custom field 9 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_10_src',
            $instance->form_data,
            __('Custom field 10 source', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __('enter static text or smart tag', 'fz_wpforms_erpnext'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_custom_10_target',
            $instance->form_data,
            __('Custom field 10 target', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __('fieldname on DocType'),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_all_fields',
            $instance->form_data,
            __('Write all answers into field', 'fz_wpforms_erpnext')
        );

        echo '</div>';
        echo '<div class="wpforms-panel-content-section wpforms-panel-content-section-fz_erpnext">';
        echo '<div class="wpforms-panel-content-section-title">' . __('Linked DocType Settings', 'fz_wpforms_erpnext') . '</div>';

        echo '<p>only required if a second ERPNext Document needs to be created.</p>';

        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype',
            $instance->form_data,
            __('Create or find linked DocType', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'Lead',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_link_type_field_name',
            $instance->form_data,
            __('Name of the Link type field', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'opportunity_from',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_link_field_name',
            $instance->form_data,
            __('Name of the Link field', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'party_name',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_fieldname_0',
            $instance->form_data,
            __('find through field', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_value_0',
            $instance->form_data,
            __('find field value', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_fieldname_1',
            $instance->form_data,
            __('additional field 1', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_value_1',
            $instance->form_data,
            __('additional field 1 value', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_fieldname_2',
            $instance->form_data,
            __('additional field 2', 'fz_wpforms_erpnext')
        );
        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_linked_doctype_value_2',
            $instance->form_data,
            __('additional field 2 value', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter static text or smart tag',
                    'fz_wpforms_erpnext'
                ),
            )
        );

        echo '<div class="wpforms-panel-content-section wpforms-panel-content-section-fz_erpnext">';
        echo '<div class="wpforms-panel-content-section-title">' . __('Items on Main DocType', 'fz_wpforms_erpnext') . '</div>';

        echo '<p>Add items to the main ERPNext Document. Only works with DocTypes that have a child table named "items".</p>';
        echo '<p>Items from the following list will always be added. (one item code per line).</p>';

        wpforms_panel_field(
            'textarea',
            'settings',
            'fz_erpnext_items_fixed',
            $instance->form_data,
            __('fixed items (one item code per line)', 'fz_wpforms_erpnext')
        );

        echo '<p>The following is a list of items that will only be added if the corresponding choice is selected in a field:</p>';
        wpforms_panel_field(
            'textarea',
            'settings',
            'fz_erpnext_items_conditional',
            $instance->form_data,
            __('conditional items (one pair [option]=>[item code] per line)', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'example choice 1=>ITEM-CODE-1',
                    'fz_wpforms_erpnext'
                ),
            )
        );

        echo '<p>The following fields will be checked for options/choices that should be converted into items on the DocType (one smart tag per line).</p>';
        wpforms_panel_field(
            'textarea',
            'settings',
            'fz_erpnext_items_conditional_source_fields',
            $instance->form_data,
            __('choice fields for conditional items', 'fz_wpforms_erpnext'),
            array(
                'smarttags'   => array(
                    'type' => 'fields',
                    'fields' => ''
                    ),
                'placeholder' => __(
                    'enter smart tags of option fields', 'fz_wpforms_erpnext'
                ),
            )
        );

        wpforms_panel_field(
            'text',
            'settings',
            'fz_erpnext_debug_level',
            $instance->form_data,
            __('debug level / verbosity', 'fz_wpforms_erpnext'),
            array(
                'placeholder' => __(
                    'enter a number [0-3]', 'fz_wpforms_erpnext'
                ),
            )
        );
        echo '<p>0: no debug messages<br>1: only errors (default)<br>2: log every request status, and errors with full requests<br>3: tell me everything!</p>';

        echo '</div>';
    }
    add_filter(
        'wpforms_form_settings_panel_content',
        'FZ_ERPNext_WPForms_Settings_content', 200
    );
}

?>
