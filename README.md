# WPForms ERPNext Connector

This is a WordPress Plugin to integrate [WPForms](https://wpforms.com) and [ERPNext](https://erpnext.com).

By using the [ERPNext REST API](https://frappe.erpnext.com/docs/user/en/api/rest), it allows you to send any WPForms submissions/entries to any ERPNext DocType.

Useful to:
- automatically create a Lead everytime somebody fills out a form on your website.
- create an Issue everytime a user submits your customer support form.
- have a WPForm to allow your customers to create an Appointment in ERPNext

## Setup

1. create a dedicated ERPNext user account only for the api access
2. create a Role that only allows creation of the DocType into which you want to feed your WPForms entries. Assign this role to the user you just created.
3. install and activate this plugin in your wordpress backend
4. open the WPForms Builder for any of your existing forms and go to settings > ERPNext
5. fill out the domain and credential fields, and select which values from the WPForm should go into which fields on the ERPNext DocType
6. Test your form and check the result in ERPNext

## Linked DocType
Sometimes you need a second Document of different DocType to exist before you can create the main Document which will receive the form data.
For example the Opportunity DocType in ERPNext needs to be linked to a Lead or Customer. The Fields under "DocTypes Settings" in the form settings under ERPNext allow you to do this.
You can specify a field of the linked DocType that is used to look for an existing Document, that is linked to the main Document if it exists.
For example Leads need unique email addresses. By specifying email_id as "find through field" a Lead with the email given in "find field value" is either created or the existing one's name is stored in the field given in "Name of the Link field".

## Items
Some DocTypes like Opportunity, Quotation or Sales Order have a child table for items.
You can automatically add items to the main Document by listing their item codes.
The plugin also allows to add items only when certain choices in WPForms are selected, you can specify a list of conditions and a set of choice fields that should be checked.

The list of conditions should look like this:
	Text of the Choice in WPForms=>ITEM-CODE

Check that the item code really exists in your ERPNext instance, if not the API will reject the request and respond with an error page. You can see the error message in wp-content/debug.log if Wordpress debugging is active.

The set of choice fields is a list of WPForms smart-tags for all the fields whose choices you want to use to conditionally add items.

All the plugin does is take the first part of a condition (before the arrow) and check if it appears in the output of the choice fields. If it does, an item with the second part as item code is added to the main document.

### Tips
- You can add several items by repeating a condition with different item-codes.
- If you join the text of two choices in a condition and join the smart-tags in the choice fields textarea on one line, you effectively get "if user selected option x in field 1 and option y in field 2 add the item." Careful: this only when the user can select only one choice in the first field!
- You can specify a debug level per form to troubleshoot individual forms while hiding messages from other forms in the wordpress debug.log. As always be careful when activating debug logging in production because the logs might contain user data.